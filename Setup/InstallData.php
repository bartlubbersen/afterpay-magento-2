<?php namespace Afterpay\Payment\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * Init
     *
     * @param EavSetup $eavSetup
     */
    public function __construct(EavSetup $eavSetup)
    {
        $this->eavSetup = $eavSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $this->eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'cocnumber',
            [
                'type' => 'static',
                'input' => 'text',
                'label' => 'CoC number',
                'required' => false,
                'system' => 0,
            ]
        );

        $setup->endSetup();
    }
}

