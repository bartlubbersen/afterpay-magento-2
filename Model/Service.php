<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Helper\Service\DataFactory as ServiceHelperFactory;
use Magento\Framework\Exception\PaymentException;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Backend\Model\Session\Quote as BackendCheckoutSession;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;

/**
 * AfterPay service model
 */
class Service
{
    /**
     * Afterpay service
     *
     * @var Afterpay
     */
    protected $_afterpayService;

    /**
     * Afterpay helper
     *
     * @var \Afterpay\Payment\Helper\Service\Data
     */
    protected $_helper;

    /**
     * Customer order
     *
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * Checkout session
     *
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * Remote IP address if unavailable in order object
     *
     * @var string
     */
    protected $_remoteAddress;

    /**
     * Payment method instance
     *
     * @var \Magento\Payment\Model\Method\AbstractMethod
     */
    protected $_paymentMethodInstance;

    /**
     * Debug helper
     *
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * Constructor
     *
     * @param Afterpay $afterpayService
     * @param ServiceHelperFactory $serviceHelperFactory
     * @param CheckoutSession $checkoutSession
     * @param BackendCheckoutSession $backendCheckoutSession
     * @param State $appState
     * @param RemoteAddress $remoteAddress
     * @param DebugHelper $debugHelper
     */
    public function __construct(
        Afterpay $afterpayService,
        ServiceHelperFactory $serviceHelperFactory,
        CheckoutSession $checkoutSession,
        BackendCheckoutSession $backendCheckoutSession,
        State $appState,
        RemoteAddress $remoteAddress,
        DebugHelper $debugHelper
    ) {
        $this->_helper = $serviceHelperFactory->create();
        $this->_afterpayService = $afterpayService;

        if ($appState->getAreaCode() == Area::AREA_ADMINHTML) {
            $this->_checkoutSession = $backendCheckoutSession;
        } else {
            $this->_checkoutSession = $checkoutSession;
        }

        $this->_remoteAddress = $remoteAddress->getRemoteAddress();
        $this->_debugHelper = $debugHelper;
    }

    /**
     * Send request to Afterpay service
     *
     * @param \Magento\Sales\Model\Order $order
     * @throws PaymentException
     */
    public function sendRequest(\Magento\Sales\Model\Order $order)
    {
        $this->_paymentMethodInstance = $order->getPayment()->getMethodInstance();

        // Payment method
        $this->_debugHelper->debug(
            $this->_paymentMethodInstance->getCode(),
            [
                'payment_method' => $this->_checkoutSession->getQuote()->getPayment()->getMethod()
            ]
        );

        $this->_order = $order;

        // Set request parameters and send request
        try {
            $this->_setOrder();

            // Debug request
            $this->_debugHelper->debug(
                $this->_paymentMethodInstance->getCode(),
                [
                    'order_request' => $this->_afterpayService->order
                ]
            );

            $this->_afterpayService->do_request(
                $this->_helper->getAuthorization($this->_paymentMethodInstance),
                $this->_helper->getModus($this->_paymentMethodInstance)
            );

            // Debug authorization and modus
            $this->_debugHelper->debug(
                $this->_paymentMethodInstance->getCode(),
                [
                    'authorization' => $this->_afterpayService->authorization,
                    'modus' => $this->_afterpayService->modus
                ]
            );

            if (!isset($this->_afterpayService->order_result->return)) {
                $this->_debugHelper->debug(
                    $this->_paymentMethodInstance->getCode(),
                    [
                        'order_result' => $this->_afterpayService->order_result
                    ],
                    true
                );

                throw new \Exception($this->_afterpayService->order_result);
            }
        } catch (PaymentException $e) {
            if ($e->getMessage()=='house_number_error') {

                // Debug house number error
                $this->_debugHelper->debug($this->_paymentMethodInstance->getCode(), $e->getMessage(), true);

                throw new PaymentException(__('The house number addition of the shipping address is missing. Please check your shipping details or contact our customer service.'));
            }
        } catch (\Exception $e) {

            // Debug authorization, modus and error message
            $this->_debugHelper->debug(
                $this->_paymentMethodInstance->getCode(),
                [
                    'authorization' => $this->_afterpayService->authorization,
                    'modus' => $this->_afterpayService->modus,
                    'technical_problem' => $e->getMessage()
                ],
                true
            );

            $this->_helper->getLogger()->critical($e->getMessage());
            throw new PaymentException(__('There is a technical problem while connecting with AfterPay. Please contact our customer service.'));
        }

        // Debug response
        $this->_debugHelper->debug(
            $this->_paymentMethodInstance->getCode(),
            [
                'order_response' => $this->_afterpayService->order_result
            ],
            true
        );

        return $this->_afterpayService->order_result->return;
    }

    /**
     * Get validation error message depending on failure code
     *
     * @param string $failureCode AfterPay failure code
     * @return string
     */
    public function getValidationErrorMsg($failureCode)
    {
        return $this->_afterpayService->check_validation_error($failureCode);
    }

    /**
     * Get rejection error message and description depending on rejection code
     *
     * @param string $rejectionCode AfterPay rejection code
     * @return mixed
     */
    public function getRejectionErrorData($rejectionCode)
    {
        return $this->_afterpayService->check_rejection_error($rejectionCode);
    }

    /**
     * Add order line
     * @todo finish parsing order object
     */
    protected function _setOrder()
    {
        /** @var \Magento\Sales\Model\Order\Address $billingAddress */
        $billingAddress = $this->_order->getBillingAddress();
        /** @var \Magento\Sales\Model\Order\Address $shippingAddress */
        $shippingAddress = $this->_order->getShippingAddress() ?: $billingAddress;
        /** @var \Magento\Sales\Model\Order\Payment $orderPayment */
        $orderPayment = $this->_order->getPayment();
        $splitBillingAddress = $this->_splitStreet($billingAddress->getStreet());
        $splitShippingAddress = $this->_splitStreet($shippingAddress->getStreet());
        $orderType = $this->_getOrderType($orderPayment->getMethod());
        $order = [
            'invoicenumber' => '', // @todo only necessary for refund and capture requests
            'ordernumber' => $this->_order->getIncrementId(),
            'creditinvoicenumber' => '', // @todo only necessary for refund requests - AF-113, 119, 120
            'bankaccountnumber' => $this->_order->getBankaccountnumber() ? $this->_order->getBankaccountnumber() : '', // @todo only necessary for the AfterPay NL Direct Debit
            'currency' => $this->_order->getOrderCurrencyCode(),
            'ipaddress' => $this->_order->getRemoteIp() ? $this->_order->getRemoteIp() : $this->_remoteAddress,
            'billtoaddress' => [
                'isocountrycode' => $billingAddress->getCountryId(),
                'city' => $billingAddress->getCity(),
                'postalcode' => $billingAddress->getPostcode(),
                'streetname' => $splitBillingAddress['streetname'],
                'housenumber' => $splitBillingAddress['housenumber'],
                'housenumberaddition' => $splitBillingAddress['houseNumberAddition'],
                'referenceperson' => [
                    'dob' => $this->_helper->formatDob($this->_order->getCustomerDob()),
                    'email' => $billingAddress->getEmail(),
                    'gender' => $this->_order->getCustomerGender(),
                    'initials' => $this->_helper->getInitials($billingAddress->getFirstname()),
                    'isolanguage' => $this->_helper->getIsoLanguage($orderPayment->getMethod()),
                    'lastname' => $billingAddress->getLastname(),
                    'phonenumber' => $billingAddress->getTelephone() ?: $this->_order->getCustomerTelephone()
                ]
            ],
            'shiptoaddress' => [
                'isocountrycode' => $shippingAddress->getCountryId(),
                'city' => $shippingAddress->getCity(),
                'postalcode' => $shippingAddress->getPostcode(),
                'streetname' => $splitShippingAddress['streetname'],
                'housenumber' => $splitShippingAddress['housenumber'],
                'housenumberaddition' => $splitShippingAddress['houseNumberAddition'],
                'referenceperson' => [
                    'dob' => $this->_helper->formatDob($this->_order->getCustomerDob()),
                    'email' => $shippingAddress->getEmail(),
                    'gender' => $this->_order->getCustomerGender(),
                    'initials' => $this->_helper->getInitials($shippingAddress->getFirstname()),
                    'isolanguage' => $this->_helper->getIsoLanguage($orderPayment->getMethod()),
                    'lastname' => $shippingAddress->getLastname(),
                    'phonenumber' => $shippingAddress->getTelephone() ?: $this->_order->getCustomerTelephone()
                ]
            ]
        ];

        if ($orderType == 'B2B') {
            $this->_afterpayService->order->company = new \stdClass();
            $this->_afterpayService->order->person = new \stdClass();
            $order['company'] = [
                'cocnumber' => $this->_order->getCustomerCocNumber(),
                'companyname' => $this->_order->getCustomerCompany(),
                'vatnumber' => $this->_order->getCustomerTaxvat()
            ];
        }

        $this->_addProductLine();
        $this->_addDiscountLine();
        $this->_addShippingFeeLine();
        $this->_addPaymentFeeLine();
        $this->_afterpayService->set_order($order, $orderType);
    }

    /**
     * Split address
     *
     * @param $address
     * @return array
     * @throws PaymentException
     */
    protected function _splitStreet($address)
    {
        $address = is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname'  => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.*?)([0-9]+)(.*)/s', $address, $matches)) {
            if ('' == $matches[1]) {
                // Number at beginning
                $ret['housenumber'] = trim($matches[2]);
                $ret['streetname']  = trim($matches[3]);
            } else {
                // Number at end
                $ret['streetname']  = trim($matches[1]);
                $ret['housenumber'] = trim($matches[2]);
                $ret['houseNumberAddition'] = trim($matches[3]);
            }
        } else {
            // No number
            throw new PaymentException(__('house_number_error'));
        }
        
        return $ret;
    }

    /**
     * Add product line
     */
    protected function _addProductLine()
    {
        $quoteItems = $this->_checkoutSession->getQuote()->getAllItems();
        foreach ($quoteItems as $item) {
            if ($this->_shouldBeSeparateLine($item)) {
                $this->_afterpayService->create_order_line(
                    $item->getSku(), // Article ID
                    $item->getQty() . ' x ' . $item->getName(), // Article description
                    '1', // Quantity
                    (string)($item->getRowTotalInclTax() * 100), // Unit price
                    $this->_helper->getAfterpayVATCategory($this->_paymentMethodInstance,
                        $item->getTaxClassId()) // VAT category
                );
            }
        }
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product, it will be added to order.
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     *
     * @return bool
     */
    protected function _shouldBeSeparateLine($item)
    {
        $bundleKey = \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE;
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== $bundleKey) {
                return false;
            }
        } elseif ($item->getProduct()->getTypeId() == $bundleKey) {
            return false;
        }
        return true;
    }

    /**
     * Add discount line
     */
    protected function _addDiscountLine()
    {
        $discountAmount = $this->_order->getDiscountAmount() * 100;
        if ($discountAmount != 0) {
            $this->_afterpayService->create_order_line(
                'Discount', // Article description
                'DISCOUNT', // Article ID
                '1', // Quantity
                (string)$discountAmount, // Unit price
                $this->_helper->getAfterpayVATCategory($this->_paymentMethodInstance, 'discount')
            );
        }
    }

    /**
     * Add shipping fee line
     */
    protected function _addShippingFeeLine()
    {
        $shippingFee = $this->_order->getShippingInclTax() * 100;
        if ($shippingFee != 0) {
            $this->_afterpayService->create_order_line(
                'Verzendkosten', // Article description
                'VERZ', // Article ID
                '1', // Quantity
                (string)$shippingFee, // Unit price
                $this->_helper->getAfterpayVATCategory($this->_paymentMethodInstance, 'shipping')
            );
        }
    }

    /**
     * Add payment fee line
     */
    protected function _addPaymentFeeLine()
    {
        $paymentFee = $this->_order->getAfterpayPaymentFee() * 100;
        if ($paymentFee != 0) {
            $this->_afterpayService->create_order_line(
                'Servicekosten AfterPay', // Article description
                'Service FEE', // Article ID
                '1', // Quantity
                (string)$paymentFee, // Unit price
                $this->_helper->getAfterpayVATCategory($this->_paymentMethodInstance, 'fee')
            );
        }
    }

    /**
     * Data helper
     *
     * @return \Afterpay\Payment\Helper\Service\Data
     */
    public function getHelper()
    {
        return $this->_helper;
    }

    /**
     * Get order type b2b or b2c
     *
     * @param $paymentMethod
     * @return string
     */
    protected function _getOrderType($paymentMethod)
    {
        return $paymentMethod == 'afterpay_nl_business_2_business' ? 'B2B' : 'B2C';
    }
}
