<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model\Sales\Quote\Total;

use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Afterpay\Payment\Model\Method\Factories;
use Magento\Framework\App\State;

/**
 * Quote including AfterPay fee total calculation
 */
class Fee extends AbstractTotal
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * Application state
     *
     * @var State
     */
    protected $_isBackend;

    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;

    /**
     * @param PriceCurrencyInterface $priceCurrency
     * @param State $appState
     * @param Factories $paymentMethodFactories
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        State $appState,
        Factories $paymentMethodFactories
    ) {
        $this->_priceCurrency = $priceCurrency;
        $this->_isBackend = $appState->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML;
        $this->_paymentMethodFactories = $paymentMethodFactories;
        $this->setCode('afterpay_payment_fee');
    }

    /**
     * Collect quote totals - only for order creation in backend
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        if (!$shippingAssignment->getItems() || !$this->_isBackend) {
            return $this;
        }

        $method = $quote->getPayment()->getMethod();
        if ($this->_paymentMethodFactories->paymentMethodExists($method)) {
            $paymentFeeAmount = $this->_paymentMethodFactories->getPaymentMethodInstance($method)->getFeeValue();
            $basePaymentFeeAmount = $paymentFeeAmount;

            $total->addTotalAmount($this->getCode(), $paymentFeeAmount);
            $total->addBaseTotalAmount($this->getCode(), $basePaymentFeeAmount);

            // Required for correct fee calculation
            $quote->setAfterpayPaymentFee($paymentFeeAmount);
        }

        return $this;
    }

    /**
     * Add payment fee totals information - only for order creation in backend
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $result = [];
        $method = $quote->getPayment()->getMethod();

        if ($this->_isBackend && $this->_paymentMethodFactories->paymentMethodExists($method)) {
            $paymentMethodInstance = $this->_paymentMethodFactories->getPaymentMethodInstance($method);
            $result = [
                'code' => $this->getCode(),
                'title' => $paymentMethodInstance->getFeeTitle(),
                'value' => $paymentMethodInstance->getFeeValue()
            ];
        }

        return $result;
    }
}
