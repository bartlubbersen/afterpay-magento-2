<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model\Method;

/**
 * AfterPay payment methods factories
 */
class Factories
{
    /**
     * AfterPay payment methods factories
     *
     * @var array
     */
    protected $_paymentMethodFactory = [
        'afterpay_nl_digital_invoice' => '_digitalInvoiceNLFactory',
        'afterpay_be_digital_invoice' => '_digitalInvoiceBEFactory',
        'afterpay_nl_business_2_business' => '_b2bNLFactory',
        'afterpay_de_digital_invoice' => '',
        'afterpay_nl_direct_debit' => '_directDebitNLFactory',
        'afterpay_nl_split_payments' => ''
    ];

    /**
     * @var DigitalInvoiceNLFactory
     */
    protected $_digitalInvoiceNLFactory;


    /**
     * @var DigitalInvoiceNLFactory
     */
    protected $_directDebitNLFactory;

    /**
     * @var DigitalInvoiceBEFactory
     */
    protected $_digitalInvoiceBEFactory;

    /**
     * @var B2BNLFactory
     */
    protected $_b2bNLFactory;

    public function __construct(
        DigitalInvoiceNLFactory $digitalInvoiceNLFactory,
        DirectDebitNLFactory $_directDebitNLFactory,
        DigitalInvoiceBEFactory $digitalInvoiceBEFactory,
        B2BNLFactory $b2BNLFactory
    ) {
        $this->_digitalInvoiceNLFactory = $digitalInvoiceNLFactory;
        $this->_directDebitNLFactory = $_directDebitNLFactory;
        $this->_digitalInvoiceBEFactory = $digitalInvoiceBEFactory;
        $this->_b2bNLFactory = $b2BNLFactory;
    }



    /**
     * Get payment method instance by payment method
     *
     * @param string $paymentMethod
     * @return DigitalInvoiceNL
     */
    public function getPaymentMethodInstance($paymentMethod)
    {
        $paymentMethodFactory = $this->_paymentMethodFactory[$paymentMethod];
        return $this->$paymentMethodFactory->create();
    }

    /**
     * Check if payment method exists
     *
     * @param $paymentMethod
     * @return bool
     */
    public function paymentMethodExists($paymentMethod)
    {
        return array_key_exists($paymentMethod, $this->_paymentMethodFactory);
    }
}
