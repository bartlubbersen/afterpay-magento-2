<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model\Method;

use Magento\Sales\Model\Order\Payment;
use Magento\Framework\App\ObjectManager;
use Magento\Developer\Helper\Data as DeveloperHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Afterpay\Payment\Helper\Service\Data as ServiceHelper;
use Magento\Backend\Model\Session\Quote as BackendCheckoutSession;
use Magento\Framework\App\State;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;

/**
 * AfterPay payment method
 */
class DigitalInvoiceNL extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'afterpay_nl_digital_invoice';

    /**
     * Payment fee code
     *
     * @var string
     */
    protected $_feeCode = 'afterpay_nl_digital_invoice_fee';

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;
    

    /**
     * Service Helper
     *
     * @var ServiceHelper
     */
    protected $_serviceHelper;

    /**
     * Magento Developer Helper
     *
     * @var \Magento\Developer\Helper\Data
     */
    protected $_devHelper;

    /**
     * Debug helper
     *
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * Checkout session
     *
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @param DebugHelper $debugHelper
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param DeveloperHelper $devHelper
     * @param ServiceHelper $serviceHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        DebugHelper $debugHelper,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        DeveloperHelper $devHelper,
        ServiceHelper $serviceHelper,
        CheckoutSession $checkoutSession,
        BackendCheckoutSession $backendCheckoutSession,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );

        $this->_serviceHelper = $serviceHelper;
        $this->_devHelper = $devHelper;
        $this->_debugHelper = $debugHelper;

        if ($context->getAppState()->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            $this->_checkoutSession = $backendCheckoutSession;
        } else {
            $this->_checkoutSession = $checkoutSession;
        }
    }


    /**
     * Check whether method is available. If billing country (shipping address is entered later on) is in allowed
     * countries list and order total is in min-max range as per config and gateway credentials are filled in,
     * it should be available.
     *
     * @param \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote|null $quote
     *
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        return $this->_credentialsValid() && parent::isAvailable($quote) && $this->_isAllowedIp() &&
            $this->_isCustomerGroupAllowed() && $this->_isAllowedShip($quote);
    }

    /**
     * Get fee title config
     *
     * @return string
     */
    public function getFeeTitle()
    {
        return __((string)$this->getConfigData('fee_title'));
    }

    /**
     * Get fee value config
     *
     * @return string
     */
    public function getFeeValueConfig()
    {
        return $this->getConfigData('fee_value');
    }

    /**
     * Get fee value
     *
     * @return float
     */
    public function getFeeValue()
    {
        $quote = $this->_checkoutSession->getQuote();
        $fee = str_replace(',', '.', $this->getFeeValueConfig());

        // Fee calculation based on percentage
        if (strpos($fee, '%') !== false) {
            $feePercentage = str_replace('%', '', $fee);

            if ($quote->getAfterpayPaymentFee()) {
                $fee = ($quote->getBaseGrandTotal() - $quote->getAfterpayPaymentFee()) * ($feePercentage / 100);
            } else {
                $fee = $quote->getBaseGrandTotal() * ($feePercentage / 100);
            }
        }

        return (float)$fee;
    }

    /**
     * Get fee code
     *
     * @return string
     */
    public function getFeeCode()
    {
        return $this->_feeCode;
    }

    /**
     * Before proceeding to offer this payment method, check that credentials are filled in on admin side.
     *
     * @return bool
     */
    protected function _credentialsValid()
    {
        if ($this->getConfigData('testmode')) {
            $attributes = [
                'testmode_merchant_id',
                'testmode_portfolio_id',
                'testmode_password',
            ];
        } else {
            $attributes = [
                'production_merchant_id',
                'production_portfolio_id',
                'production_password',
            ];
        }

        foreach ($attributes as $code) {
            if (!$this->getConfigData($code)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Is allowed IP or not
     *
     * @return bool
     */
    protected function _isAllowedIp()
    {
        return $this->getConfigData('restrict') ?  $this->_devHelper->isDevAllowed() : true;
    }

    /**
     * Is customer group allowed
     *
     * @return bool
     */
    protected function _isCustomerGroupAllowed()
    {
        if ($this->getConfigData('allowspecificgroup')) {
            $specificGroupIds = $this->getConfigData('specificgroup');
            return $this->_serviceHelper->isGroupAllowed($specificGroupIds);
        }

        return true;
    }

    /**
     * Is allowed payment for shipping method or not
     *
     * @param \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote|null $quote
     * @return bool
     */
    protected function _isAllowedShip($quote)
    {
        $config = $this->getConfigData('excludeships');
        if ($config === null) {
            return true;
        }
        $methods = explode(',', $config);
        $current = $quote->getShippingAddress()->getShippingMethod();

        return !in_array($current, $methods);
    }

    /**
     * Overriding function from Magento\Payment\Model\Checks\CanUseForCountry since we cannot validate for a country
     * which has not been set yet (default checkout does not ask for billing country before fetching available payment
     * methods). This is only done for Virtual/Downloadable product types, rest should follow default flow.
     *
     * @param int $country
     *
     * @return bool
     */
    public function canUseForCountry($country)
    {
        $quote = $this->_checkoutSession->getQuote();
        $virtualType = \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL;
        $downloadableType = \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE;
        foreach($quote->getAllItems() as $item) {
            $productType = $item->getProduct()->getTypeId();
            if ($productType !== $virtualType && $productType !== $downloadableType) {
                // Fall back to original canUseForCountry method
                if ($this->getConfigData('allowspecific') == 1) {
                    $availableCountries = explode(',', $this->getConfigData('specificcountry'));
                    if (!in_array($country, $availableCountries)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function canRefund()
    {
        return $this->_serviceHelper->isRefundEnabled();
    }
}
