<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Model\Config\Provider;

use Magento\Checkout\Model\ConfigProviderInterface;
use Afterpay\Payment\Model\Method\DigitalInvoiceNL as PaymentMethod;

/**
 * Payment method configuration provider
 */
class DigitalInvoiceNL implements ConfigProviderInterface
{
    /**
     * @var PaymentMethod
     */
    protected $_paymentMethod;

    /**
     * @param PaymentMethod $paymentMethod
     * @codeCoverageIgnore
     */
    public function __construct(
        PaymentMethod $paymentMethod
    ) {
        $this->_paymentMethod = $paymentMethod;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                $this->_paymentMethod->getCode() => [
                    'code'                      => $this->_paymentMethod->getCode(),
                    'title'                     => $this->_paymentMethod->getConfigData('title'),
                    'description'               => $this->_paymentMethod->getConfigData('description'),
                    'allowed_countries'         => $this->_paymentMethod->getConfigData('specificcountry'),
                    'fee_title'                 => $this->_paymentMethod->getFeeTitle(),
                    'fee_value'                 => $this->_paymentMethod->getFeeValueConfig(),
                    'success'                   => $this->_paymentMethod->getConfigData('success'),
                    'validation_failure'        => $this->_paymentMethod->getConfigData('validation_failure'),
                    'testmode'                  => $this->_paymentMethod->getConfigData('testmode'),
                    'testmode_label'            => \Afterpay\Payment\Helper\Service\Data::TEST_MODE_LABEL,
                    'terms_and_conditions'      => $this->_paymentMethod->getConfigData('terms_and_conditions')
                ]
            ],
        ];
    }
}
