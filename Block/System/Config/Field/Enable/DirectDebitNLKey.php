<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Block\System\Config\Field\Enable;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * AfterPay adv. configuration enable
 */
class DirectDebitNLKey extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magento\Framework\View\Helper\Js
     */
    protected $_jsHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Url $url
     * @param \Magento\Framework\View\Helper\Js $jsHelper
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Url $url,
        \Magento\Framework\View\Helper\Js $jsHelper,
        \Magento\Directory\Helper\Data $directoryHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_url = $url;
        $this->_jsHelper = $jsHelper;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * Return script and button for adv. configuration enabling
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        /** @var  $html */
        $html = '<button id="afterpay_advanced_direct_debit_configuration_nl" class="button afterpay_configure close" type="button"><span class="state-closed">Enable Advanced Settings</span></button>';
        /** @var $jsString */
        $jsString = '
            var md5_field = jQuery("#payment_us_afterpay_nl_direct_debit_md5k"),
                afterpay_conf = jQuery("#row_payment_us_afterpay_nl_direct_debit_advanced_settings"),
                cookieName = "advancedConfigNL",
                inputRows = jQuery("#row_payment_us_afterpay_nl_direct_debit_md5k, #row_payment_us_afterpay_nl_direct_debit_enable_afterpay_configuration");
            md5_field.val("");
            if (jQuery.cookie(cookieName) == 1) {
                inputRows.hide();
            } else {
                afterpay_conf.css("display","none");
            }
            jQuery("#afterpay_advanced_direct_debit_configuration_nl").click(function () {
                md5_field.removeClass("required-entry _required mage-error");
                if ("hc35aDjCDu7esWE" == md5_field.val()) {
                    afterpay_conf.css("display","");
                    jQuery.cookie(cookieName, 1);
                    inputRows.hide();
                } else {
                    md5_field.addClass("required-entry _required mage-error");
                }
            });';

        return $html . $this->_jsHelper->getScript(
        'require([\'jquery\'], function(jQuery){jQuery(document).ready( function() {' . $jsString . '}); });'
        );
    }

}
