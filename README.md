# AfterPay module for Magento 2 #

The official Magento 2 module for the AfterPay payment method. This module offers a direct connection with the AfterPay payment service.

## Installation ##

### Step 1. Check permissions ###

Before module installation, make sure that magento2 is installed on your web server.
The web server user must have write access to the following files and directories:

* var
* app/etc
* pub

### Step 2. Run composer require and update ###

Open terminal, change dir to project root/ folder, and run the following command to install and update the AfterPay module and dependencies:

```
composer require afterpay/afterpay-module
```

### Step 4. Redeploy static content, update Magento and renew cache ###

Open terminal, change dir to project root/ folder, and run following commands, which will move all contents from vendor to pub/static folder:

* 'sudo rm -R pub/static' - this command will delete pub/static folder, this is required in order to update static files
* 'sudo php bin/magento setup:static-content:deploy' - this command will move all vendor files to appropriate pub/static folders
* 'sudo php bin/magento setup:upgrade' (if you run into permissions error, change permissions for appropriate folders and rerun command) - this command will run all install/upgrade scripts
* 'sudo php bin/magento cache:flush' - this command will flush all caches

At this point module should be installed, and you can proceed with module configuration.

For more information please contact your business consultant at AfterPay: support@afterpay.nl