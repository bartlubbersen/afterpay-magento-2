/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'jquery',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, $, quote) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Afterpay_Payment/checkout/summary/afterpay_nl_digital_invoice_fee'
            },
            afterpayConfig: '',

            initialize: function() {
                this._super();
                this.afterpayConfig = this.getAfterpayConfig();
            },

            isVisible: function () {
                var paymentMethod = quote.getPaymentMethod()();

                if (this.getFeeValue() == 0) {
                    return false;
                }

                if (paymentMethod && paymentMethod.method != 'afterpay_nl_digital_invoice') {
                    return this.removePaymentFee();
                } else if (paymentMethod) {
                    return this.addPaymentFee();
                }

                return false;
            },

            addPaymentFee: function () {
                var quoteTotals = quote.totals(),
                    codes = [
                        'afterpay_payment_fee',
                        'grand_total'
                    ],
                    feeValue = this.getFeeValue();

                // Add payment fee to totals if payment fee was no already added
                if (!quoteTotals.afterpay_nl_digital_invoice_fee) {
                    quoteTotals.afterpay_payment_fee = feeValue;
                    quoteTotals.afterpay_nl_digital_invoice_fee = feeValue;
                    quoteTotals.base_grand_total *= 1;
                    quoteTotals.base_grand_total += feeValue;
                    quoteTotals.grand_total *= 1;
                    quoteTotals.grand_total += feeValue;

                    $.each(quoteTotals.total_segments, function () {
                        if ($.inArray(this.code, codes) != -1) {
                            this.value += feeValue;
                        }
                    });

                    quote.setTotals(quoteTotals);
                }

                return true;
            },

            removePaymentFee: function () {
                var quoteTotals = quote.totals(),
                    codes = [
                        'afterpay_payment_fee',
                        'grand_total'
                    ],
                    feeValue = this.getFeeValue();

                // Remove payment fee from totals if payment fee was already added
                if (quoteTotals.afterpay_nl_digital_invoice_fee) {
                    quoteTotals.afterpay_payment_fee = 0;
                    quoteTotals.afterpay_nl_digital_invoice_fee = 0;
                    quoteTotals.base_grand_total -= feeValue;
                    quoteTotals.grand_total -= feeValue;

                    $.each(quoteTotals.total_segments, function () {
                        if ($.inArray(this.code, codes) != -1) {
                            this.value -= feeValue;
                        }
                    });

                    quote.setTotals(quoteTotals);
                }

                return false;
            },

            getFeeTitle: function () {
                return this.afterpayConfig.fee_title;
            },

            getFeeValue: function () {
                var quoteTotals = quote.totals(),
                    fee = this.afterpayConfig.fee_value.replace(',', '.'),
                    feePercentage;

                // Fee calculation based on percentage
                if (fee.indexOf('%') !== -1) {
                    feePercentage = fee.replace('%', '');

                    if (quoteTotals.afterpay_payment_fee) {
                        fee = (quoteTotals.base_grand_total - quoteTotals.afterpay_payment_fee) * (feePercentage / 100);
                    } else {
                        fee = quoteTotals.base_grand_total * (feePercentage / 100);
                    }
                }

                return parseFloat(fee);
            },

            getFormattedFeeValue: function () {
                return this.getFormattedPrice(this.getFeeValue());
            },

            getAfterpayConfig: function () {
                if (window.checkoutConfig.payment) {
                    var afterpayConfig = window.checkoutConfig.payment.afterpay_nl_digital_invoice;
                    if (afterpayConfig) {
                        return afterpayConfig;
                    }
                }
                return null;
            }
        });
    }
);