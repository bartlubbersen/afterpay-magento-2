/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage',
        'mage/url',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function (quote, urlBuilder, storage, url, errorProcessor, customer, fullScreenLoader) {
        'use strict';

        return function (paymentData, redirectOnSuccess, messageContainer) {
            var serviceUrl,
                payload;

            redirectOnSuccess = redirectOnSuccess !== false;

            /** Checkout for guest and registered customer. */
            if (!customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/guest-carts/:quoteId/payment-information', {
                    quoteId: quote.getQuoteId()
                });
                payload = {
                    cartId: quote.getQuoteId(),
                    email: quote.guestEmail,
                    paymentMethod: paymentData,
                    billingAddress: quote.billingAddress()
                };
            } else {
                serviceUrl = urlBuilder.createUrl('/carts/mine/payment-information', {});
                payload = {
                    cartId: quote.getQuoteId(),
                    paymentMethod: paymentData,
                    billingAddress: quote.billingAddress()
                };
            }

            fullScreenLoader.startLoader();

            return storage.post(
                serviceUrl, JSON.stringify(payload)
            ).done(
                function (response, status, jqXHR) {
                    if (jqXHR.getResponseHeader('X-Redirect-Url')) {
                        window.location.replace(jqXHR.getResponseHeader('X-Redirect-Url'));
                    }
                    else if (redirectOnSuccess) {
                        switch (paymentData.method) {
                            case 'afterpay_nl_digital_invoice':
                                if (window.checkoutConfig.payment.afterpay_nl_digital_invoice.success !== null) {
                                    window.location.replace(url.build(window.checkoutConfig.payment.afterpay_nl_digital_invoice.success));
                                } else {
                                    window.location.replace(url.build('checkout/onepage/success/'));
                                }
                                break;
                            case 'afterpay_be_digital_invoice':
                                if (window.checkoutConfig.payment.afterpay_be_digital_invoice.success !== null) {
                                    window.location.replace(url.build(window.checkoutConfig.payment.afterpay_be_digital_invoice.success));
                                } else {
                                    window.location.replace(url.build('checkout/onepage/success/'));
                                }
                                break;
                            case 'afterpay_nl_business_2_business':
                                if (window.checkoutConfig.payment.afterpay_nl_business_2_business.success !== null) {
                                    window.location.replace(url.build(window.checkoutConfig.payment.afterpay_nl_business_2_business.success));
                                } else {
                                    window.location.replace(url.build('checkout/onepage/success/'));
                                }
                                break;
                            default:
                                window.location.replace(url.build('checkout/onepage/success/'));
                                break;
                        }
                    }
                }
            ).fail(
                function (response) {
                    if (window.checkoutConfig.payment[paymentData.method]['validation_failure']
                        && window.checkoutConfig.payment[paymentData.method]['validation_failure'] !== null
                        && response.responseJSON.parameters
                        && response.responseJSON.parameters.validation_failure === 1
                    ) {
                        var redirect_url = window.checkoutConfig.payment[paymentData.method]['validation_failure'];
                        window.location.replace(url.build(redirect_url));
                    } else {
                        fullScreenLoader.stopLoader();
                        errorProcessor.process(response, messageContainer);
                    }
                }
            );
        };
    }
);