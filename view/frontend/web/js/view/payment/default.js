/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'afterpay_nl_digital_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_digital_invoice'
            },
            {
                type: 'afterpay_nl_direct_debit',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_direct_debit'
            },
            {
                type: 'afterpay_be_digital_invoice',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_be_digital_invoice'
            },
            {
                type: 'afterpay_nl_business_2_business',
                component: 'Afterpay_Payment/js/view/payment/method-renderer/afterpay_nl_business_2_business'
            }
        );
        return Component.extend({});
    }
);