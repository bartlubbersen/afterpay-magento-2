/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote'
    ],
    function (ko, Component, $, checkoutData, quote) {
        'use strict';

        return Component.extend({
            afterpayConfig: '',
            defaults: {
                template: 'Afterpay_Payment/payment/afterpay_nl_business_2_business',
                termsAndConditions: false,
                cocNumber: '',
                vatNumber: '',
                companyName: '',
                telNumber: ''
            },

            initialize: function () {
                var self = this;
                self._super();
                self.afterpayConfig = self.getAfterpayConfig();
                self.termsAndConditions = ko.observable();
                self.cocNumber = ko.observable(this.getCocNumber());
                self.vatNumber = ko.observable(this.getVatNumber());
                self.companyName = ko.observable(this.getCompanyName());
                self.telNumber = ko.observable(this.getTelNumber());
            },

            getCode: function () {
                return 'afterpay_nl_business_2_business';
            },

            isActive: function () {
                return true;
            },

            isAvailable: function () {
                return true;
            },

            getTitle: function () {
                return this.afterpayConfig.title;
            },

            getDescription: function () {
                return this.afterpayConfig.description;
            },

            isPlaceOrderActionAllowedAfterpay: function () {
                return this.validateCountries();
            },

            validateCountries: function () {
                var billingAddress = false;
                if (checkoutData.getSelectedBillingAddress() === null) {
                    if (checkoutData.getShippingAddressFromData() === null) {
                        // 'Use same address' has been checked and user hasn't explicitly selected any shipping method,
                        // falling back to default shipping address.
                        $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                            if (address.default_billing) {
                                billingAddress = address;
                            }
                        }.bind(this));
                    } else {
                        // 'Use same address' has been checked and user has entered shipping info
                        billingAddress = checkoutData.getShippingAddressFromData();
                    }
                } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-address') {
                    // New billing address entered
                    billingAddress = checkoutData.getNewCustomerBillingAddress();
                } else {
                    // Predefined address selected as custom billing address
                    var selectedAddressId = parseInt(checkoutData.getSelectedBillingAddress().substring('customer-address'.length));
                    $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                        if (parseInt(address.id) === selectedAddressId) {
                            billingAddress = address;
                        }
                    }.bind(this));
                }
                return this.validateCountryCode(billingAddress);
            },

            validateCountryCode: function (address) {
                if (this.afterpayConfig && address) {
                    var allowedCodes = this.afterpayConfig.allowed_countries.split(',');
                    return (allowedCodes.indexOf(address.country_id) >= 0);
                }
                return false;
            },

            getAfterpayConfig: function () {
                if (window.checkoutConfig.payment) {
                    var afterpayConfig = window.checkoutConfig.payment.afterpay_nl_business_2_business;
                    if (afterpayConfig) {
                        return afterpayConfig;
                    }
                }
                return null;
            },

            getTestmodeLabel: function () {
                if (this.isTestmode()) {
                    return window.checkoutConfig.payment.afterpay_nl_business_2_business.testmode_label;
                }
            },

            isTestmode: function () {
                return parseInt(window.checkoutConfig.payment.afterpay_nl_business_2_business.testmode);
            },

            getData: function () {
                var self = this,
                    result = {
                        'method': self.item.method,
                        'po_number': null,
                        'additional_data': null
                    };

                // Add terms and conditions
                if (self.canShowConditions()) {
                    if (result.additional_data) {
                        result.additional_data.terms_and_conditions = self.termsAndConditions();
                    } else {
                        result.additional_data = {
                            terms_and_conditions: self.termsAndConditions()
                        }
                    }
                }

                // Add coc number
                if (self.cocNumber()) {
                    if (result.additional_data) {
                        result.additional_data.coc_number = self.cocNumber();
                    } else {
                        result.additional_data = {
                            coc_number: self.cocNumber()
                        }
                    }
                }

                // Add vat number
                if (self.vatNumber()) {
                    if (result.additional_data) {
                        result.additional_data.vat_number = self.vatNumber();
                    } else {
                        result.additional_data = {
                            vat_number: self.vatNumber()
                        }
                    }
                }

                // Add company name
                if (self.companyName()) {
                    if (result.additional_data) {
                        result.additional_data.company_name = self.companyName();
                    } else {
                        result.additional_data = {
                            company_name: self.companyName()
                        }
                    }
                }

                // Add tel number
                if (self.telNumber()) {
                    if (result.additional_data) {
                        result.additional_data.customer_telephone = self.telNumber();
                    } else {
                        result.additional_data = {
                            customer_telephone: self.telNumber()
                        }
                    }
                }

                return result;
            },

            canShowConditions: function () {
                return this.afterpayConfig.terms_and_conditions != 0;
            },

            getConditionsLink: function () {
                return 'https://www.afterpay.nl/nl/algemeen/betalen-met-afterpay/algemene-voorwaarden-nl';
            },

            getVatNumber: function () {
                if (window.customerData && window.customerData.taxvat) {
                    return window.customerData.taxvat
                } else {
                    return quote.billingAddress() ? quote.billingAddress().vatId : null;
                }
            },

            getCocNumber: function() {
                try {
                    return window.customerData.custom_attributes.cocnumber.value;
                } catch (e) {
                    return null;
                }
            },
            
            getCompanyName: function () {
                return quote.billingAddress() ? quote.billingAddress().company : null;
            },

            getTelNumber: function () {
                return quote.billingAddress() ? quote.billingAddress().telephone : null;
            }
        });
    }
);