/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
define([
    'jquery',
    'mage/translate'
], function ($, $t) {
    'use strict';

    return function() {
        var $el = $('#edit_form'),
            config,
            baseUrl,
            order,
            payment;

        if (!$el.length || !$el.data('order-config')) {
            return;
        }

        config = $el.data('order-config');
        baseUrl = $el.data('load-base-url');

        order = new AdminOrder(config);
        order.setLoadBaseUrl(baseUrl);

        // Override - reload totals upon payment method switch
        order.switchPaymentMethod = function(method) {
            $('#edit_form').trigger('changePaymentMethod', [method]);
            this.setPaymentMethod(method);
            var data = {};
            data['order[payment_method]'] = method;
            this.loadArea(['card_validation', 'totals'], true, data);
            afterPayCustomFields.init();
        };

        // Override - enable custom fields
        order.setPaymentMethod = function(method) {
            if (this.paymentMethod && $('payment_form_'+this.paymentMethod)) {
                var form = 'payment_form_'+this.paymentMethod;
                [form + '_before', form, form + '_after'].each(function(el) {
                    var block = $(el);
                    if (block) {
                        block.hide();
                        block.select('input', 'select', 'textarea').each(function(field) {
                            field.disabled = true;
                        });
                    }
                });
            }

            if(!this.paymentMethod || method){
                $('order-billing_method_form').select('input', 'select', 'textarea').each(function(elem){
                    if(elem.type != 'radio' && $.inArray(method, afterPayCustomFields.methodList) == -1) elem.disabled = true;
                })
            }

            if ($('payment_form_'+method)){
                jQuery('#' + this.getAreaId('billing_method')).trigger('contentUpdated');
                this.paymentMethod = method;
                var form = 'payment_form_'+method;
                [form + '_before', form, form + '_after'].each(function(el) {
                    var block = $(el);
                    if (block) {
                        block.show();
                        block.select('input', 'select', 'textarea').each(function(field) {
                            field.disabled = false;
                            if (!el.include('_before') && !el.include('_after') && !field.bindChange) {
                                field.bindChange = true;
                                field.paymentContainer = form;
                                field.method = method;
                                field.observe('change', this.changePaymentData.bind(this))
                            }
                        },this);
                    }
                },this);
            }
        };

        payment = {
            switchMethod: order.switchPaymentMethod.bind(order)
        };

        window.order = order;
        window.payment = payment;

        var afterPayCustomFields = {
            methodList: [
                'afterpay_nl_digital_invoice',
                'afterpay_be_digital_invoice',
                'afterpay_nl_business_2_business'
            ],

            init: function () {
                this.addCustomerFieldsToPayment();
            },

            // Function to update the days based on the current values of month and year
            updateNumberOfDays: function (paymentMethod) {
                var $days = $('#' + paymentMethod + '_dob_days'),
                    month = $('#' + paymentMethod + '_dob_months').val(),
                    year = $('#' + paymentMethod + '_dob_years').val(),
                    days = this.daysInMonth(month, year);

                $days.append($('<option />').val('').html($t('Day')));

                for (var i = 1; i < days + 1 ; i++){
                    $days.append($('<option />').val(i).html(i));
                }
            },

            // Helper function
            daysInMonth: function (month, year) {
                return new Date(year, month, 0).getDate();
            },

            // Add custom fields to payment methods block
            addCustomerFieldsToPayment: function () {
                var self = this;

                // Add custom fields for each payment method from the list
                self.methodList.each(function (paymentMethod) {
                    if (paymentMethod == 'afterpay_nl_business_2_business') {
                        self.addB2BFields(paymentMethod);
                    } else {
                        self.addGenderFields(paymentMethod);
                        self.addDobFields(paymentMethod);
                    }
                });
            },

            addGenderFields: function (paymentMethod) {
                var $paymentMethodEl = $('#p_method_' + paymentMethod),
                    customerGenderEl;

                if (!$('#gender').length) {
                    if (!$('#' + paymentMethod + '_gender').length && $paymentMethodEl.length) {
                        customerGenderEl =
                            '<div class="afterpay field">' +
                            '<label class="label" for="' + paymentMethod + '_gender"><span>' + $t('Gender') + '</span></label>' +
                            '<div class="admin__field-control control">' +
                            '<select id="' + paymentMethod + '_gender" name="payment[customer_gender]" title="' + $t('Gender') + '" class="validate-select select admin__control-select"><option value="1">' + $t('Male') + '</option> <option value="2" selected="selected">' + $t('Female') + '</option></select>' +
                            '</div>' +
                            '</div>';

                        $paymentMethodEl.parent().next().append(customerGenderEl);
                    }
                }
            },

            addDobFields: function (paymentMethod) {
                var $paymentMethodEl = $('#p_method_' + paymentMethod),
                    self = this;

                if (!$('#dob').length) {
                    if (!$('#' + paymentMethod + '_dob').length && $paymentMethodEl.length) {
                        var $dobMonths,
                            $dobYears,
                            customerDobEl;

                        customerDobEl =
                            '<div class="afterpay field admin__field-option">' +
                            '<label class="label"><span>' + $t('Date of Birth') + '</span></label>' +
                            '<div id="' + paymentMethod + '_dob" class="admin__field-control control">' +
                            '<select id="' + paymentMethod + '_dob_days" class="select admin__control-select"></select>' +
                            '<select id="' + paymentMethod + '_dob_months" class="select admin__control-select"></select>' +
                            '<select id="' + paymentMethod + '_dob_years" class="select admin__control-select"></select>' +
                            '<input type="hidden" name="payment[customer_dob]" value=""/>' +
                            '</div>' +
                            '</div>';

                        $paymentMethodEl.parent().next().append(customerDobEl);

                        $dobMonths = $('#' + paymentMethod + '_dob_months');
                        $dobYears = $('#' + paymentMethod + '_dob_years');

                        // Populate our months select box
                        $dobMonths.append($('<option />').val('').html($t('Month')));
                        for (var i = 1; i < 13; i++) {
                            $dobMonths.append($('<option />').val(i).html(i));
                        }

                        // Populate our years select box
                        $dobYears.append($('<option />').val('').html($t('Year')));
                        for (i = new Date().getFullYear(); i > 1900; i--) {
                            $dobYears.append($('<option />').val(i).html(i));
                        }

                        // Populate our Days select box
                        self.updateNumberOfDays(paymentMethod);

                        // Listen for change events
                        $('#' + paymentMethod + '_dob_years, #' + paymentMethod + '_dob_months').change(function () {
                            self.updateNumberOfDays(paymentMethod);
                            self.setDob(paymentMethod);
                        });

                        $('#' + paymentMethod + '_dob_days').change(function () {
                            self.setDob(paymentMethod);
                        });
                    }
                }
            },

            setDob: function (paymentMethod) {
                var month = $('#' + paymentMethod + '_dob_months').val(),
                    day = $('#' + paymentMethod + '_dob_days').val(),
                    year = $('#' + paymentMethod + '_dob_years').val(),
                    dob = $('input[name="payment[customer_dob]"]');

                if (month && day && year) {
                    if (month < 10) {
                        month = '0' + month;
                    }

                    if (day < 10) {
                        day = '0' + day;
                    }

                    dob.val(month + '/' + day + '/' + year);
                } else {
                    dob.val(null);
                }
            },

            addB2BFields: function (paymentMethod) {
                var $paymentMethodEl = $('#p_method_' + paymentMethod),
                    customerCOCNumberEl =
                        '<div class="afterpay field">' +
                        '<label class="label" for="' + paymentMethod + '_coc_number"><span>' + $t('COC Number') + '</span></label>' +
                        '<div class="admin__field-control control">' +
                        '<input type="text" id="' + paymentMethod + '_coc_number" name="payment[coc_number]" title="' + $t('COC Number') + '" class="input-text admin__control-text"/>' +
                        '</div>' +
                        '</div>',
                    customerVATNumberEl =
                        '<div class="afterpay field admin__field-option">' +
                        '<label class="label" for="' + paymentMethod + '_vat_number"><span>' + $t('VAT Number') + '</span></label>' +
                        '<div class="admin__field-control control">' +
                        '<input type="text" id="' + paymentMethod + '_vat_number" name="payment[vat_number]" title="' + $t('VAT Number') + '" class="input-text admin__control-text"/>' +
                        '</div>' +
                        '</div>';

                if (!$('#' + paymentMethod + '_coc_number').length && $paymentMethodEl.length) {
                    $paymentMethodEl.parent().next().append(customerCOCNumberEl);
                }

                if (!$('#' + paymentMethod + '_vat_number').length && $paymentMethodEl.length && !$('#taxvat').length && !('#order-billing_address_vat_id').length) {
                    $paymentMethodEl.parent().next().append(customerVATNumberEl);
                }
            }
        };

        afterPayCustomFields.init();
    }
});