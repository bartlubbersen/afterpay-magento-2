<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\ManagerInterface;
use Afterpay\Payment\Model\Method\Factories;

/**
 * Add payment fee to payment
 */
class AddPaymentFeeToPayment implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;

    /**
     * @param ManagerInterface $eventManager
     * @param Factories $paymentMethodFactories
     */
    public function __construct(
        ManagerInterface $eventManager,
        Factories $paymentMethodFactories
    ) {
        $this->_eventManager = $eventManager;
        $this->_paymentMethodFactories = $paymentMethodFactories;
    }

    /**
     * Add payment fee to payment
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $observer->getEvent()->getPayment();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();

        if ($this->_paymentMethodFactories->paymentMethodExists($payment->getMethod())) {
            $orderPaymentFeeAmount = $order->getAfterpayPaymentFee();
            $baseOrderPaymentFeeAmount = $order->getBaseAfterpayPaymentFee();

            $payment->setAfterpayPaymentFee(0);
            $payment->setBaseAfterpayPaymentFee(0);

            $payment->setAfterpayPaymentFee($orderPaymentFeeAmount);
            $payment->setBaseAfterpayPaymentFee($baseOrderPaymentFeeAmount);
        }

        return $this;
    }
}
