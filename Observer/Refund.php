<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */


namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\State;
use Afterpay\Afterpay as Afterpay;
use Magento\Checkout\Model\Session as CheckoutSession;
use Afterpay\Payment\Helper\Service\DataFactory as ServiceHelperFactory;
use \Magento\Framework\Message\ManagerInterface as Messages;

/**
 * Class Capture
 * @package Afterpay\Payment\Observer
 *
 */
class Refund implements ObserverInterface
{

    const REFUND_RESULT_SUCCESS = '0';

    const REFUND_RESULT_REFUSED = '3';

    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Service model
     */
    protected $_afterpayService;

    /**
     * Payment method inctance
     */
    protected $_methodInstance;

    /**
     * Helper
     *
     * @var \Afterpay\Payment\Helper\Service\Data
     */
    protected $_helper;

    /**
     * Checkout session
     *
     * @var
     */
    protected $_session;

    /**
     * Messages
     * @var
     */
    protected $_messages;


    /**
     * Refund constructor.
     * @param Afterpay $afterpayService
     * @param ServiceHelperFactory $serviceHelperFactory
     * @param CheckoutSession $checkoutSession
     * @param Messages $messages
     */
    public function __construct(
        Afterpay $afterpayService,
        ServiceHelperFactory $serviceHelperFactory,
        CheckoutSession $checkoutSession,
        Messages $messages
    ) {
        $this->_afterpayService = $afterpayService;
        $this->_helper = $serviceHelperFactory->create();
        $this->_session = $checkoutSession;
        $this->_messages = $messages;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $allowedMethods = $this->_helper->getAllowedMethods();
        /**
         * @var $creditmemo \Magento\Sales\Model\Order\Creditmemo
         */
        $creditmemo = $observer->getCreditmemo();

        /** @var  $invoice \Magento\Sales\Model\Order\Invoice */
        $invoice = $creditmemo->getInvoice();

        /** @var  $order \Magento\Sales\Model\Order */
        $order = $creditmemo->getOrder();

        $this->_methodInstance = $order->getPayment()->getMethodInstance();

        $creditmemoGrandTotal = round($creditmemo->getGrandTotal(), 2);
        $invoiceGrandTotal = round($invoice->getGrandTotal(), 2);
        if ($creditmemoGrandTotal == $invoiceGrandTotal) {
            $this->_processFullRefund($creditmemo);
            return true;
        }

        if (!in_array($this->_methodInstance->getCode(), $allowedMethods)
            || !$this->_methodInstance->canRefundPartialPerInvoice()
            || !$this->_methodInstance->canRefund()

        ) {
            return false;
        }

        $this->_afterpayService->set_ordermanagement('refund_partial');
        $this->_gatherRefundData($creditmemo, $invoice, $order);

        $this->_afterpayService->do_request(
            $this->_helper->getAuthorization($this->_methodInstance),
            $this->_helper->getModus($this->_methodInstance)
        );
        $response = $this->_afterpayService->order_result->return;
        $this->_checkResponse($response, $order);

        return true;
    }


    /**
     * Gathering required information for Afterpay
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @param \Magento\Sales\Model\Order $order
     */
    protected function _gatherRefundData($creditmemo, $invoice, $order)
    {
        $result= [];

        $result['invoicenumber'] = $invoice->getIncrementId();
        $result['ordernumber'] = $order->getIncrementId();
        $result['creditinvoicenumber'] = $creditmemo->getIncrementId();

        foreach ($creditmemo->getAllItems() as $item) {
            if ($item->getPriceInclTax() == 0 
                || $item->hasParentItemId() 
                || empty($item) 
                || !$item->getQty()
                || $item->getProductType() == 'bundle') {
                continue;
            }
            $this->_addOrderLine($item);
        }

        if ($creditmemo->getAdjustment()) {
            $this->_addAdjustmentLine($creditmemo, $order);
        }

        if ($creditmemo->getAfterpayServiceTaxRefund()) {
            $afterpayPaymentFee = $order->getAfterpayPaymentFee();
            $serviceFee = $order->formatPrice($afterpayPaymentFee);
            $this->_addFeeLine($afterpayPaymentFee);
            $order->addStatusHistoryComment(__(
                sprintf('Afterpay service fee has been refunded: %s', $serviceFee))
            )->save();
            $order->setAfterpayPaymentFee(0);
            $order->setBaseAfterpayPaymentFee(0);
            $order->save();
        }

        $discountAmount = $creditmemo->getDiscountAmount();
        if ($discountAmount < 0) {
            $this->_addDiscountLine($discountAmount);
        }

        $shippingFee = $creditmemo->getShippingInclTax();
        
        if ($shippingFee > 0) {
            $this->_addShippingFeeLine($shippingFee);
        }
        
        $this->_afterpayService->set_order($result, self::ORDER_MANAGEMENT_CODE);
    }

    /**
     * @param \stdClass $response
     * @param \Magento\Sales\Model\Order $order
     * @throws \Exception
     */
    protected function _checkResponse($response, $order)
    {
        if ($response->resultId == self::REFUND_RESULT_SUCCESS) {
            $order->addStatusHistoryComment(__('This order has been refunded by AfterPay'))->save();
        } else {
            $order->addStatusHistoryComment(__('AfterPay refund attempt has failed'))->save();
            $this->_messages->addError(__($response->messages[0]['message']));
            throw new \Exception(__('AfterPay refund attempt has failed'));
        }
        $order->save();
    }


    /**
     * Add service fee line to service object
     *
     * @param string $paymentFee
     */
    protected function _addFeeLine($paymentFee)
    {
        $this->_afterpayService->create_order_line(
            'Servicekosten AfterPay', // Article description
            'Refund: Service FEE', // Article ID
            '1', // Quantity
            '-' . $paymentFee * 100, // Unit price
            $this->_helper->getAfterpayVATCategory($this->_methodInstance, 'fee')
        );
    }

    /**
     * Add discount line to service object
     *
     * @param string $discountAmount
     */
    protected function _addDiscountLine($discountAmount)
    {
        $this->_afterpayService->create_order_line(
            'Discount', // Article description
            'DISCOUNT', // Article ID
            '1', // Quantity
            (string)(abs($discountAmount) * 100), // Unit price
            $this->_helper->getAfterpayVATCategory($this->_methodInstance, 'discount')
        );
    }

    /**
     * Add shipping fee line
     */
    protected function _addShippingFeeLine($shippingFee)
    {
        $shippingFee = $shippingFee * 100;
        if ($shippingFee != 0) {
            $this->_afterpayService->create_order_line(
                'Verzendkosten', // Article description
                'VERZ', // Article ID
                '1', // Quantity
                (string)$shippingFee, // Unit price
                $this->_helper->getAfterpayVATCategory($this->_methodInstance, 'shipping')
            );
        }
    }

    /**
     * Add order line to service object
     *
     * @param \Magento\Sales\Model\Order\Creditmemo\Item $item
     */
    protected function _addOrderLine($item)
    {
        $sku = $item->getSku();
        $name = __('Refund:') . ' ' . $item->getName();
        $qty = $item->getQty();
        $price = (int)round($item->getPriceInclTax() * 100 * -1, 0);
        $taxClassId = $this->_helper->getTaxClassId($item->getProductId());
        $taxCategory = $this->_helper->getAfterpayVATCategory($this->_methodInstance,
            $taxClassId);

        $this->_afterpayService->create_order_line($sku, $name, $qty, $price, $taxCategory);
    }

    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @param \Magento\Sales\Model\Order\ $order
     */
    protected function _addAdjustmentLine($creditmemo, $order)
    {
        $adjustment = $creditmemo->getAdjustment();
        if ($creditmemo->getAfterpayServiceTaxRefund()) {
            $afterpayFee = $order->getAfterpayPaymentFee();
            $adjustmentToAdd = $adjustment - $afterpayFee;
        } else {
            $adjustmentToAdd = $adjustment;
        }

        if ($adjustment > 0) {
            $this->_afterpayService->create_order_line(
                'Adjustment line', // Article description
                'ADJUSTMENT', // Article ID
                '1', // Quantity
                (string)($adjustmentToAdd * 100 * -1), // Unit price
                '4'
            );
        }
    }

    /**
     * Performing full refund
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     */
    protected function _processFullRefund($creditmemo)
    {
        $aporder = [];

        $aporder['invoicenumber'] = $creditmemo->getInvoice()->getIncrementId();
        $aporder['ordernumber'] = $creditmemo->getOrder()->getIncrementId();
        $aporder['creditinvoicenumber'] = $creditmemo->getIncrementId();

        $this->_afterpayService->set_ordermanagement('refund_full');

        $this->_afterpayService->set_order($aporder, self::ORDER_MANAGEMENT_CODE);

        $this->_afterpayService->do_request(
            $this->_helper->getAuthorization($this->_methodInstance),
            $this->_helper->getModus($this->_methodInstance)
        );

        $response = $this->_afterpayService->order_result->return;
        $this->_checkResponse($response, $creditmemo->getOrder());
    }
}