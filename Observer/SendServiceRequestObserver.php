<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\DataObject;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\PaymentException;
use Magento\Framework\Event\ManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Backend\Model\Session\Quote as QuoteSession;
use Magento\Framework\App\State;
use Afterpay\Payment\Model\ServiceFactory as AfterpayServiceFactory;
use Afterpay\Payment\Model\Method\Factories;
use Magento\Framework\Webapi\Rest\Response as RestResponse;
use Magento\Framework\Webapi\Rest\Request as RestRequest;
use \Magento\Framework\Message\ManagerInterface as Messages;

/**
 * Send request to AfterPay service
 */
class SendServiceRequestObserver implements ObserverInterface
{
    /**
     * Genders
     */
    const GENDER_MALE       = 1;

    const GENDER_FEMALE     = 2;

    /**
     * AfterPay service response results
     */
    const RESULT_SUCCESS            = 0; // Success

    const RESULT_EXCEPTION          = 1; // Exception in the request/response

    const RESULT_VALIDATION_ERROR   = 2; // Validation errors in the request

    const RESULT_REJECTED           = 3; // Rejected (payment refused) by AfterPay

    const RESULT_PENDING            = 4; // Pending

    /**
     * AfterPay payment methods codes
     */
    const PAYMENT_METHOD_CODE_BE        = 'afterpay_be_digital_invoice';

    const PAYMENT_METHOD_CODE_DB_NL        = 'afterpay_nl_direct_debit';

    const PAYMENT_METHOD_CODE_B2B_NL    = 'afterpay_nl_business_2_business';

    /**
     * @var ManagerInterface
     */
    protected $_eventManager;


    /**
     * @var $_messages
     */
    protected $_messages;

    /**
     * Factory for AfterPay service model
     *
     * @var \Afterpay\Payment\Model\ServiceFactory
     */
    protected $_afterpayServiceFactory;

    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;
    
    /**
     * Rest response object
     *
     * @var RestResponse
     */
    protected $_restResponse;

    /**
     * Rest request object
     *
     * @var RestRequest
     */
    protected $_restRequest;

    /**
     * Checkout quote
     *
     * @var QuoteSession|\Magento\Quote\Model\Quote
     */
    protected $_quote;

    /**
     * Is backend or frontend
     *
     * @var bool
     */
    protected $_isBackend;

    /**
     * Reuse original gender between functions
     *
     * @var int
     */
    protected $_customerGender;
    /**
     * @param ManagerInterface $eventManager
     * @param AfterpayServiceFactory $afterpayServiceFactory
     * @param Factories $paymentMethodFactories
     * @param CheckoutSession $checkoutSession
     * @param State $appState
     * @param QuoteSession $quoteSession
     * @param RestResponse $response
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        ManagerInterface $eventManager,
        AfterpayServiceFactory $afterpayServiceFactory,
        Factories $paymentMethodFactories,
        CheckoutSession $checkoutSession,
        QuoteSession $quoteSession,
        State $appState,
        RestResponse $response,
        RestRequest $request,
        Messages $messages
    ) {
        $this->_eventManager = $eventManager;
        $this->_afterpayServiceFactory = $afterpayServiceFactory;
        $this->_paymentMethodFactories = $paymentMethodFactories;
        $this->_messages = $messages;

        if ($appState->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            $this->_isBackend = true;
            $this->_quote = $quoteSession->getQuote();
        } else {
            $this->_isBackend = false;
            $this->_quote = $checkoutSession->getQuote();
        }

        $this->_restResponse = $response;
        $this->_restRequest = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @throws PaymentException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $observer->getEvent()->getPayment();
        /** @var \Afterpay\Payment\Model\Service $afterpayService */
        $afterpayService = $this->_afterpayServiceFactory->create();

        if ($this->_paymentMethodFactories->paymentMethodExists($payment->getMethod())) {
            $order = $payment->getOrder();
            $order = $this->_adjustOrderData($order);
            $response = $afterpayService->sendRequest($order);
            $errorMsg = __('There is a technical problem while connecting with AfterPay. Please contact our customer service.');

            switch ($response->resultId) {
                case self::RESULT_SUCCESS:
                    $order->setState($order::STATE_PROCESSING);
                    $order->setStatus(\Afterpay\Payment\Helper\Service\Data::ORDER_STATUS_PROCESSING);
                    $payment->setBaseAmountAuthorized($order->getBaseTotalDue());
                    $payment->capture();
                    break;
                case self::RESULT_EXCEPTION:
                    throw new PaymentException($errorMsg);
                    break;
                case self::RESULT_VALIDATION_ERROR:
                    $redirect = $payment->getMethodInstance()->getConfigData('validation_failure');
                    // Passing validation error to json response
                    $validationFailure = ($redirect) ? array('validation_failure' => 1) : null;

                    //Fix for BE payment method

                    if ($payment->getMethod() == 'afterpay_be_digital_invoice') {
                        $response = $this->_adjustFailureFields($response);
                    }

                    // For multiple failures pick first failure
                    if (is_array($response->failures)) {
                        if ($redirect) {
                            foreach ($response->failures as $failure) {
                                $this->_messages->addError($afterpayService->getValidationErrorMsg($failure->failure));
                            }
                        }
                        throw new PaymentException(__($afterpayService->getValidationErrorMsg($response->failures[0]->failure), $validationFailure));
                    }
                    $validationErrorMsg = $afterpayService->getValidationErrorMsg($response->failures->failure);
                    if ($redirect) {
                        $this->_messages->addError($validationErrorMsg);
                    }
                    throw new PaymentException(__($validationErrorMsg, $validationFailure));
                    break;
                case self::RESULT_REJECTED:
                    if (isset($response->rejectCode)) {
                        $rejectionErrorData = $afterpayService->getRejectionErrorData($response->rejectCode);
                        $errorMsg = __($rejectionErrorData['description']);
                    }
                    throw new PaymentException($errorMsg);

                    break;
                case self::RESULT_PENDING:
                    $order->setState($order::STATE_NEW);

                    if ($payment->getMethod() == self::PAYMENT_METHOD_CODE_BE) {
                        $order->setStatus($payment->getMethodInstance()->getConfigData('pending_order_status'));
                    } else {
                        $order->setStatus(\Afterpay\Payment\Helper\Service\Data::ORDER_STATUS_PENDING);
                    }

                    if (isset($response->extrafields->nameField, $response->extrafields->valueField)
                        && $response->extrafields->nameField == 'redirectUrl'
                    ) {
                        $this->_restResponse->setHeader('X-Redirect-Url', $response->extrafields->valueField);
                    }
                    
                    break;
                default:
                    throw new PaymentException($errorMsg);
                    break;
            }

            // Provide sent order info for further use
            $this->_eventManager->dispatch(
                'afterpay_send_service_request_after', ['order' => $order, 'gender' => $this->_customerGender]
            );

        }

        return $this;
    }

    /**
     * Adjust order and add additional data, which is required by AfterPay
     *
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Sales\Model\Order
     * @throws PaymentException
     */
    protected function _adjustOrderData(\Magento\Sales\Model\Order $order)
    {
        $payment = $this->_quote->getPayment();
        $requestData = $this->_restRequest->getRequestData();
        $paymentData = new DataObject(isset($requestData['paymentMethod']['additional_data']) ? $requestData['paymentMethod']['additional_data'] : []);
        $billingAddress = $this->_quote->getBillingAddress();


        //Credit Card Validation

        if ($payment->getMethod() == self::PAYMENT_METHOD_CODE_DB_NL) {
            $bankaccountNumber = $paymentData->getBankaccountnumber();
            if (!$bankaccountNumber) {
                throw new PaymentException(__('Please enter bank account number.'));
            } else {
                $order->setBankaccountnumber($bankaccountNumber);
            }
        }

        // Telephone number validation
        if ($billingAddress->getTelephone()) {
            $telephone = $billingAddress->getTelephone();
        } else {
            $telephone = $paymentData->getCustomerTelephone();
        }

        // Billing Company > Payment Company
        if ($billingAddress->getCompany()) {
            $companyName = $billingAddress->getCompany();
        } else {
            $companyName = $paymentData->getCompanyName();
        }

        if (!$companyName && $payment->getMethod() == self::PAYMENT_METHOD_CODE_B2B_NL) {
            throw new PaymentException(__('Please enter company name.'));
        }

        if (!$telephone) {
            throw new PaymentException(__('Please enter telephone number.'));
        }

        $order->setCustomerCompany($companyName);
        $order->setCustomerTelephone($telephone);

        // Add data to order depending on payment method
        if ($payment->getMethod() != self::PAYMENT_METHOD_CODE_B2B_NL) {
            $customer = $this->_quote->getCustomer();
            // Use customers gender if it is set, use payment method gender otherwise
            $customerGender = ($customer->getGender() > 0 && $customer->getGender() < 3) ? $customer->getGender() : $paymentData->getCustomerGender();
            $this->_customerGender = $customerGender;
            // Use customers dob if it is set, use payment method dob otherwise
            $customerDob = $customer->getDob() ?: $paymentData->getCustomerDob();

            // Gender validation
            if (!$customerGender || !($customerGender > 0 && $customerGender < 3)) {
                throw new PaymentException(__('Please select gender.'));
            }

            // DOB validation
            if (!$customerDob) {
                throw new PaymentException(__('Please select date of birth.'));
            }

            // Format customer gender for AfterPay request object
            switch ($customerGender) {
                case self::GENDER_MALE:
                    $formattedCustomerGender = 'M';
                    break;
                case self::GENDER_FEMALE:
                    $formattedCustomerGender = 'V';
                    break;
                default:
                    $formattedCustomerGender = '';
                    break;
            }

            $order->setCustomerGender($formattedCustomerGender);
            $order->setCustomerDob($customerDob);
        } else {
            $cocNumber = $paymentData->getCocNumber();

            // Billing VAT > Customer VAT > Payment VAT
            if ($billingAddress->getVatId()) {
                $vatNumber = $billingAddress->getVatId();
            } elseif ($this->_quote->getCustomerTaxvat()) {
                $vatNumber = $this->_quote->getCustomerTaxvat();
            } else {
                $vatNumber = $paymentData->getVatNumber();
            }

            if (!$cocNumber) {
                throw new PaymentException(__('Please enter COC number.'));
            }

            if (!$vatNumber) {
                throw new PaymentException(__('Please enter VAT number.'));
            }

            $order->setCustomerCocNumber($cocNumber);
            $order->setCustomerTaxvat($vatNumber);
        }

        // Conditions validation
        if ($payment->getMethodInstance()->getConfigData('terms_and_conditions')
            && !$paymentData->getTermsAndConditions()
            && !$this->_isBackend
        ) {
            throw new PaymentException(__('Please agree to all the terms and conditions.'));
        }

        return $order;
    }

    /**
     * @param $response
     * @return mixed
     */
    protected function _adjustFailureFields($response)
    {
        if (is_array($response->failures)) {
            foreach ($response->failures as &$failures) {
                $fieldParts = explode('.', $failures->failure);
                $failures->failure = $fieldParts[0] . '.' . $failures->fieldname . '.' . $fieldParts[1];
            }
        } else {
            $failure = $response->failures->failure;
            $fieldName = $response->failures->fieldname;
            $fieldParts = explode('.', $failure);
            $response->failures->failure = $fieldParts[0] . '.' . $fieldName . '.' . $fieldParts[1];
        }

        return $response;
    }
}
